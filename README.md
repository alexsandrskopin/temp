class Sberbank
{
    public function registerOrder ($paymentId)
    {

        $payment = Payment::objects()->filter(['id' => $paymentId])->get();

        $transaction = new Transaction();
        $transaction->payment = $payment;
        $transaction->save();

        $transactionId = $transaction->id;

        $returnURL = Phact::app()->request->getHostInfo() . Phact::app()->router->url('main:processing', ['paymentId' => $paymentId]);

        $data = null;
        $data = $this->request('payment/rest/register.do', [
            'userName' => Phact::app()->settings->get("Sberbank.api_key"),
            'password' => Phact::app()->settings->get('Sberbank.password'),
            'amount' => ($payment->cost)*100,
            'language' => 'ru',
            'orderNumber' => $transactionId,
            'returnUrl' => $returnURL,
        ]);
        
        if (isset($data['orderId'])){
            $transaction->key_transaction = $data['orderId'];
            $transaction->status = Transaction::STATUS_PENDING;
            $transaction->save();

            return $data['formUrl'];
        }


        return null;
    }

    public function getDomain()
    {

        //return "https://3dsec.sberbank.ru"; //test domain
        return "https://securepayments.sberbank.ru";
    }

     public function updateStatus($transaction)
     {
        $response = $this->getStatus($transaction->key_transaction);
        if (isset($response['OrderStatus'])) {
            $sberStatus = $response['OrderStatus'];
            $pendingStatuses = [0, 1, 5];
            $successStatuses = [2];
            $errorStatuses = [3, 4, 6];

            $status = Transaction::STATUS_PENDING;
            if (in_array($sberStatus, $successStatuses)) {
                $status = Transaction::STATUS_SUCCESS;
            } elseif (in_array($sberStatus, $errorStatuses)) {
                $status = Transaction::STATUS_FAIL;
            }

            $transaction->status = $status;
            $transaction->save();
        }
     }

    public function getStatus ($keyTransaction) {

        return $this->request('payment/rest/getOrderStatus.do', [
            'orderId' => $keyTransaction,
            'userName' => Phact::app()->settings->get("Sberbank.api_key"),
            'password' => Phact::app()->settings->get('Sberbank.password'),
        ]);
    }

    public function request($action, $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->getDomain() . '/' . $action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);

        curl_close ($ch);
        if ($response) {

            try  {
                return json_decode($response, true);
            } catch (\Exception $exception) {
                // Error
            }
        }
        return null;
    }
}